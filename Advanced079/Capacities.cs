﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Smod2;
using Smod2.API;

namespace Advanced079
{
    class Capacities
    {
        public static void FakeNTFAnnouncement()
        {
            string st = "abcdefghijklmnopqrstuvwxyz";
            char c = st[new Random().Next(0, st.Length)];

            PluginManager.Manager.Server.Map.AnnounceNtfEntrance(PluginManager.Manager.Server.Round.Stats.SCPAlive, new Random().Next(0, 9), c);
        }

        public static void FakeCIAnnouncement()
        {
            PluginManager.Manager.Server.Map.AnnounceCustomMessage("Attention . chaosinsurgency detected");
        }

        public static void FakeSCPDeadAnnouncement()
        {
            Player[] pscps = PluginManager.Manager.Server.GetPlayers().Where(p => p.TeamRole.Role != Role.SCP_079 && p.TeamRole.Team == Smod2.API.Team.SCP).ToArray();
            if (pscps.Any())
            {
                Player target = pscps[new Random().Next(0, pscps.Count())];

                switch (target.TeamRole.Role)
                {
                    case Role.SCP_049:
                        PluginManager.Manager.Server.Map.AnnounceScpKill("049");
                        break;

                    case Role.SCP_096:
                        PluginManager.Manager.Server.Map.AnnounceScpKill("096");
                        break;

                    case Role.SCP_106:
                        PluginManager.Manager.Server.Map.AnnounceScpKill("106");
                        break;

                    case Role.SCP_173:
                        PluginManager.Manager.Server.Map.AnnounceScpKill("173");
                        break;

                    case Role.SCP_939_53:
                        PluginManager.Manager.Server.Map.AnnounceScpKill("939");
                        break;

                    case Role.SCP_939_89:
                        PluginManager.Manager.Server.Map.AnnounceScpKill("939");
                        break;
                }
            }
        }

        public static void CloseAllDoors()
        {
            foreach(Smod2.API.Door d in PluginManager.Manager.Server.Map.GetDoors().Where(d => string.IsNullOrWhiteSpace(d.Name)))
            {
                d.Open = false;
            }
        }

        public static void TotalBlackout()
        {
            foreach (Room r in PluginManager.Manager.Server.Map.Get079InteractionRooms(Scp079InteractionType.CAMERA).Where(r => r.ZoneType == ZoneType.HCZ || r.ZoneType == ZoneType.LCZ))
            {
                r.FlickerLights();
            }
        }

        public static string[] GetDoorsStatus()
        {
            List<string> doorsStatus = new List<string>();

            foreach(Smod2.API.Door d in PluginManager.Manager.Server.Map.GetDoors().Where(d => !string.IsNullOrWhiteSpace(d.Name)))
            {
                doorsStatus.Add("<color=#fff>" + d.Name + " : </color>" + (d.Open ? "<color=green>[OUVERT]</color>" : "<color=red>[FERME]</color>") + (d.Locked ? " <color=red>[VEROUILLE]</color>" : ""));
            }

            return doorsStatus.ToArray();
        }

        public static string[] GetGeneratorsStatus()
        {
            List<string> generatorStatus = new List<string>();

            foreach (Generator gen in PluginManager.Manager.Server.Map.GetGenerators())
            {
                generatorStatus.Add($"<color=#fff>{gen.Room.RoomType.ToString()} :</color> {(gen.Locked ? "" : "<color=red>[DEVEROUILLE]</color>")} {(gen.HasTablet ? "<color=red>[ACTIF]</color>" + $" <color=yellow>Temps restant : {gen.TimeLeft} secondes</color>" : "")} {(gen.Engaged ? "<color=red>[ENGAGE]</color>" : "")}");
            }

            return generatorStatus.ToArray();
        }

    }
}
