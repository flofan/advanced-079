﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Smod2;
using Smod2.Attributes;

namespace Advanced079
{

    [PluginDetails(author = "Flo - Fan", configPrefix = "a079", description = "", id = "a079.flo", langFile = "advanced079", name = "Advanced 079", SmodMajor = 3, SmodMinor = 4, SmodRevision = 1, version = "Dev")]

    public class Main : Plugin
    {
        public override void OnDisable()
        {
            this.Info("Plugin disabled.");
        }

        public override void OnEnable()
        {
            this.Info("Plugin enabled, Version : " + this.Details.version );
        }

        public override void Register()
        {
            AddEventHandlers(new Handlers.OnCallCommand(this));

            AddConfig(new Smod2.Config.ConfigSetting("a079_enabled", true, true, "Enabled the plugin"));

            AddConfig(new Smod2.Config.ConfigSetting("a079_fakemtf_mana", 50f,true, ""));
            AddConfig(new Smod2.Config.ConfigSetting("a079_fakemtf_level", 3f,true, ""));

            AddConfig(new Smod2.Config.ConfigSetting("a079_fakeci_mana", 50f, true, ""));
            AddConfig(new Smod2.Config.ConfigSetting("a079_fakeci_level", 3f, true, ""));

            AddConfig(new Smod2.Config.ConfigSetting("a079_fakescp_mana", 60f, true, ""));
            AddConfig(new Smod2.Config.ConfigSetting("a079_fakescp_level", 3f, true, ""));

            AddConfig(new Smod2.Config.ConfigSetting("a079_closealldoor_mana", 200f, true, ""));
            AddConfig(new Smod2.Config.ConfigSetting("a079_closealldoor_level", 5f, true, ""));

            AddConfig(new Smod2.Config.ConfigSetting("a079_totalblackout_mana", 200f, true, ""));
            AddConfig(new Smod2.Config.ConfigSetting("a079_totalblackout_level", 5f, true, ""));

            AddConfig(new Smod2.Config.ConfigSetting("a079_getgenerators_mana", 30f, true, ""));
            AddConfig(new Smod2.Config.ConfigSetting("a079_getgenerators_level", 0f, true, ""));

            AddConfig(new Smod2.Config.ConfigSetting("a079_getdoors_mana", 20f, true, ""));
            AddConfig(new Smod2.Config.ConfigSetting("a079_getdoors_level", 0f, true, ""));

        }
    }
}
